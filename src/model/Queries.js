/* ------------------------------- QUERIES DB ------------------------------- */

class Queries {
    db;
    datafile = require('./Database');
    rxjs = require('rxjs');

    subjectEmails = new this.rxjs.BehaviorSubject();

    constructor() {
        this.db = this.datafile.Database.getInstance();
        
        this.setEmails((data) => {
            this.subjectEmails.next(data);
        });
    }

    setEmails(cb) {
        this.db.getConnection().query("SELECT * FROM emails", (err, result, fields) => {
            if(err) throw err;
            cb(result);
        });
    }

    getEmails(cb) {
        setTimeout(() => {
            cb(this.subjectEmails.asObservable());
        }, 100);
    }

    createEmail(cb, data) {
        this.db.getConnection().query("INSERT INTO emails(subject, content, isread, sentdate) VALUES (?,?,?,?)", [data.subject, data.content, data.isRead, data.sentDate], (err, result, fields) => {
            if(err) throw err;
            if(result.affectedRows > 0)
                cb(result);
        })
    }

    deleteEmail(cb, id) {
        this.db.getConnection().query("DELETE FROM emails WHERE id = ?", [id], (err, result, fields) => {
            if(err) throw err;
            if(result.affectedRows > 0)
                cb(result);
        })
    }

    verifyCredential(cb, data) {
        this.db.getConnection().query("SELECT * FROM users WHERE username = ?", [data.username], (err, result, fields) => {
            if(err) throw err;
            if(result.length > 0) {
                if(result[0].username === data.username && result[0].password === data.password)
                    cb(result[0]);
            } else {
                cb(false);
            }
            
        })
    }
}

exports.module = { Queries };