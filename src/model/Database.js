/* ----------------------------- DATABASE CLASS ----------------------------- */

class Database {
    static instance;
    connection;
    mysql;
    
    constructor() {
        this.mysql = require('mysql');
        this.connection = this.createConnection();
    }

    static getInstance() {
        if(this.instance === undefined) {
            this.instance = new Database();
            return this.instance;
        } else {
            return this.instance;
        }
    }

    createConnection() {
        const connection = this.mysql.createConnection({
            host: '172.17.0.4',
            user: 'Strackz',
            password: 'Strackz123',
            database: 'my_db'
        })
    
        return connection;
    }

    getConnection() {
        return this.connection;
    }
}

module.exports = { Database };