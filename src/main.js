const express = require('express');
const app = express();
const port = 5656;
const Queries = require('./model/Queries');
const BODY = require('body-parser');
const JWT = require('jsonwebtoken');

/* ------------------------------- MIDDLEWARES ------------------------------ */

app.use(BODY.json());


app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', '*');
    res.setHeader('Access-Control-Allow-Methods', '*');
	res.setHeader('Content-Type', 'application/json');

    next();
})

/* ------------------------------ ALL THE GETS ------------------------------ */

app.get('/messages', (req, res) => {
    const DB = new Queries.module.Queries();

    DB.getEmails((observable) => {
        observable.subscribe((data) => {
            if(data.length > 0)
                res.status(200).send(data);
            else
                res.status(200).send({status: 404});
        })
    });
});

app.get('/messages/:id', (req, res) => {
    const DB = new Queries.module.Queries();

    DB.getEmails((observable) => {
        observable.subscribe((data) => {
            if(data.length > 0){
                res.status(200).send(data.filter(email => +email.id === +req.params.id)[0]);
            } else
                res.status(200).send({status: 404});
        })
    });
})


/* ------------------------------ ALL THE POSTS ----------------------------- */

app.post('/messages', (req, res) => {
    const DB = new Queries.module.Queries();

    DB.createEmail((data) => {
        if(data)
            res.status(200).send({status: 200});
        else
            res.status(200).send({status: 404});
    }, req.body)
})


/* ----------------------------- ALL THE DELETES ---------------------------- */

app.delete('/messages/:id', (req, res) => {
    const DB = new Queries.module.Queries();

    DB.deleteEmail((data) => {
        if(data)
            res.status(200).send({status: 200});
        else
            res.status(200).send({status: 404});
    }, req.params.id)
})

/* --------------------------------- EXTRAS --------------------------------- */

app.listen(port, () => {
    console.log(`listening on http://localhost:${port}`)
});



/* ------------------------------- AUTH SYSTEM ------------------------------ */

const SECRETKEY = 'My super secret phrase';

app.use((req, res, next) => {
    let token = JSON.parse(req.headers.authorization);

    console.log(token);
    next();
});

app.post('/auth', (req, res) => {
    const DB = new Queries.module.Queries();

    DB.verifyCredential((data) => {
        // Verify if the user exists, if the user doesnt exists we send a status code 404 (Not Found)
        if(data) {
            // Personal Informations that will be stored inside our token.
            const USER = {
                username: data.username,
                userPermissions: {
                    reader: Boolean(data.reader),
                    writer: Boolean(data.writer)
                },
                isConnected: true
            }

            // Generation of the Token to be sent to the user
            const TOKEN = JWT.sign(USER, 'My super secret phrase', {
                algorithm: "HS256",
                expiresIn: 300,
            });

            res.status(200).json({jwt: TOKEN});
        } else {
            res.status(200).json({status: 404});
        }
    }, req.body);

})

app.get('/auth/unpack', (req, res) => {
    // Parsing of the Token that the client is currently holding
    const TOKEN = JSON.parse(JSON.parse(req.headers.authorization));

    try {
        // We try to unpack the token, if the token exists we will send it back to the client, with the data decrypted
        const DECODED = JWT.verify(TOKEN.jwt, SECRETKEY);

        // All the generic data of the user.
        res.status(200).json(DECODED);
    } catch(err) {
        res.status(200).json({status: 404}); 
    }
})

app.get('/auth/unpack/perms', (req, res) => {
    // Parsing of the Token that the client is currently holding
    const TOKEN = JSON.parse(JSON.parse(req.headers.authorization));

    try {
        // We try to unpack the token, if the token exists we will send it back to the client, with the data decrypted
        const DECODED = JWT.verify(TOKEN.jwt, SECRETKEY);

        // In this case we will only send the permissions of writing and reading of the user.
        res.status(200).json(DECODED.userPermissions);
    } catch(err) {
        res.status(200).json({status: 404}); 
    }
})

